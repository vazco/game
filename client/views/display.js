'use strict';


function render_board(tiles) {
    Session.set('tiles', tiles);
  for (var i = 0; i <= 3; i++) {
    for (var j = 0; j <= 3; j++) {
      var t = tiles[i][j];

      if (tiles[i][j] !== 0) {
        var block = Blaze.toHTMLWithData(Template.tile, {row: i, col: j, tile: t});
        block = $(block).addClass(App.util.tile_class(t));
        // (Here there be magic numbers)
        block = $(block).css({
          left: 22 + (j * 92),
          top: 22 + (i * 130)
        });

        $('.board').append(block);
      }
    }
  }
}

function render_next(next_tile) {
  $('.next-block .tile').removeClass('red')
                  .removeClass('blue')
                  .removeClass('number')
                  .removeClass('bonus');
  $('.next-block .tile').addClass(App.util.tile_class(next_tile));
}

function render_lost(total) {
 console.log(total);
}

function animate_move(obj, direction, tmpl) {
  var board = obj.board;
  var moved = obj.moved;

  var movement,coords;

  switch(direction) {
    case GameMoves.LEFT:
      movement = function(c) {
        return {top: c.top, left: c.left - 92};
      };
      coords = function(i, j) {
        return String(i) + String(j - 1);
      };
    break;

    case GameMoves.RIGHT:
      movement = function(c) {
        return {top: c.top, left: c.left + 92};
      };
      coords = function(i, j) {
        return String(i) + String(j + 1);
      };
    break;

    case GameMoves.UP:
      movement = function(c) {
        return {top: c.top - 130, left: c.left};
      };
      coords = function(i, j) {
        return String(i - 1) + String(j);
      };
    break;

    case GameMoves.DOWN:
      movement = function(c) {
        return {top: c.top + 130, left: c.left};
      };
      coords = function(i, j) {
        return String(i + 1) + String(j);
      };
    break;
  }

    tmpl.$('.tile').css('zIndex', 10);

  _.each(moved, function(t) {
    var el = tmpl.$('[data-coords=' + String(t.i) + String(t.j) + ']');

    var old_coords = {top: parseInt(el.css('top')), left: parseInt(el.css('left'))};
    var new_coords = movement(old_coords);

    el.css('zIndex', 100);
    el.animate({
      top: new_coords.top,
      left: new_coords.left
    }, 200, 'easeOutQuart', function() {
        tmpl.$('[data-coords=' + coords(t.i, t.j) + ']').remove();
      el.attr('data-coords', coords(t.i, t.j));
      el.removeClass('blue');
      el.removeClass('red');
      el.removeClass('number');
      el.addClass(App.util.tile_class(t.t));
      el.html(t.t);

      // el.effect('bounce', {distance: 30, times: 3});
    });
  });
}

function animate_new_tile(coords, direction, tmpl) {
  var origin, next_tile = coords.t;

  switch(direction) {
    case GameMoves.LEFT:
      origin = function(top, left) {
        return {top: top, left: left + 92};
      };
    break;

    case GameMoves.RIGHT:
      origin = function(top, left) {
        return {top: top, left: left - 92};
      };
    break;

    case GameMoves.UP:
      origin = function(top, left) {
        return {top: top + 130, left: left};
      };
    break;

    case GameMoves.DOWN:
      origin = function(top, left) {
        return {top: top - 130, left: left};
      };
    break;
  }

  var block = Blaze.toHTMLWithData(Template.tile, {row: coords.i, col: coords.j, tile: next_tile});
  block = $(block).addClass(App.util.tile_class(next_tile));

  var top = 22 + (coords.i * 130);
  var left = 22 + (coords.j * 92);
  var origins = origin(top, left);

  block.css({
    left: origins.left,
    top: origins.top
  });
    tmpl.$('.board').append(block);

  block.animate({
    top: top,
    left: left
  }, 200, 'easeOutQuart');
}

App.display = {
  render_board: render_board,
  render_next: render_next,
  render_lost: render_lost,
  animate_move: animate_move,
  animate_new_tile: animate_new_tile
};