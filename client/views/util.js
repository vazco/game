'use strict';
var deckInit = [1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3];
var currentDec = [];

function randomTile(tiles) {
    var t;
  // Bonus draw
  var bonus = _.some(_.flatten(tiles), function(t) {return (t >= 48);} );
  if (bonus && (Math.random() <= 1/21)) {
    var highest = _.max(_.flatten(tiles));
    var size = Math.log(highest / 3) / Math.log(2) - 3; // Help what is math
    var bonus_deck = _(size).times(function(n) {
      return 6 * Math.pow(2, n);
    });

    t = _.sample(bonus_deck);
    return t;
  }

  // Normal draw
  if (_.isEmpty(currentDec)) {
    currentDec = _.shuffle(deckInit);
  }

  t = _.first(currentDec);
  currentDec = _.rest(currentDec);
  return t;
}

// Helper to compute tile class
function tile_class(tile) {
  if (tile === 1) {
    return 'blue';
  }
  else if (tile === 2) {
    return 'red';
  }
  else if (tile === 3) {
    return 'number';
  }
  else {
    return 'bonus';
  }
}

App.util = {
  random_tile: randomTile,
  tile_class: tile_class
};