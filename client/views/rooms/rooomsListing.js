'use strict';

Template.roomsList.helpers({
    getRooms: function(){
        var status = (Template.currentData() || {}).status;

        return GameRooms.find({status: status});
    }
});