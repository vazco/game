'use strict';


Template.roomGamePanel.helpers({
    'canEdit': function(){
        var userId = UniUsers.getLoggedInId();
        return userId && (this.ownerId === userId || UniUsers.isAdminLoggedIn());
    }
});

Template.roomGamePanel.events({
    'click .js-start': function(){
        this.startGame();
    },
    'click .js-cancel': function(){
        this.closeGame();
    }
});

AutoForm.hooks({
    insertGameRoom: {
        onSuccess: function(formType, id) {
            Router.go('room', {_id: id});
        }
    }
});

Template.registerHelper('GameRooms', function(){
    return GameRooms;
});

Template.roomCards.helpers({
    getPlayersForCards: function(){
        var players;
        var self = this;
        Tracker.nonreactive(function(){
            players = self.getPlayers().fetch();
            var loggedInId = UniUsers.getLoggedInId();
            //loggedInId always first
            if(loggedInId){
                var me = UniUtils.findKey(players, function(o){
                    return o._id === loggedInId;
                });
                if(me){
                    var doc = players[me];
                    players[me] = null;
                    players.unshift(doc);
                    players = _.compact(players);
                }
            }

        });
        return players;
    },
    inGame: function(){
        var self = this;
        var result;
        Tracker.nonreactive(function(){
            var loggedInId = UniUsers.getLoggedInId();
            if(_.isArray(self.players)){
                result =_.some(self.players, function(id){
                    return loggedInId && id === loggedInId;
                });
            }
        });
        return result;
    }
});

Template.roomCardsPoints.helpers({
    getPoints: function(){
        var id = this.roomId;
        var room = GameRooms.findOne(id);
        return UniUtils.get(room, 'boardCurrentMatrix.'+this.user._id+'.points');
    }
});

Template.roomNew.events({
    'click .js-start-game-alone': function(){
        var user = UniUsers.getLoggedIn();
        GameRooms.insert({
            name: 'Single game - '+ user.getName(),
            status: GameRooms.STATUS_STARTED
        }, function(err, id){
            if(err){
                console.error(err);
            } else{
                Tracker.autorun(function(c){
                    var game = GameRooms.findOne({_id: id});
                    if(game){
                        game.startGame();
                        _.delay(function(){
                            Router.go('room.game', {_id: id});
                        }, 100);
                        c.stop();
                    }
                });

            }
        });
    }
});

Template.loseGame.onRendered(function(){
    var data = Template.currentData();
    var room = GameRooms.findOne({_id: data._id}, {fields: {boardCurrentMatrix: 1}});
    if(room && !UniUtils.get(room, 'boardCurrentMatrix.'+UniUsers.getLoggedInId()+'.isEnd')){
        this.autorun(function(c){
            var room = GameRooms.findOne({_id: data._id}, {fields: {boardCurrentMatrix: 1}});
            if(room && UniUtils.get(room, 'boardCurrentMatrix.'+UniUsers.getLoggedInId()+'.isEnd')){
                $('.js-lose-game').modal('show');
                c.stop();
            }
        });
    }
});

Template.loseGame.helpers({
    getGamePoints: function(){
        var room = GameRooms.findOne(this._id);
        return room && UniUtils.get(room, 'boardCurrentMatrix.'+UniUsers.getLoggedInId()+'.points');
    },
    getAllUserPoints: function(){
        var user = UniUsers.getLoggedIn();
        return user && user.points;
    }
});