'use strict';
/************************************************************
 * basic controller for lb,
 * All controller on this site should inherit from this one
 ************************************************************/
window.BasicController = RouteController.extend({
    initLoader: function(){
        if(!this.ready()){
            Session.set('loading', true);
            console.time('loading '+this.path);
        } else{
            console.timeEnd('loading '+this.path);
            Session.set('loading', false);
        }
    },
    action: function(){
        var self = this;
        Tracker.autorun(function(){
            self.initLoader.call(self);
        });
        this.render();
    }
});