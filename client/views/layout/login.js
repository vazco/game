'use strict';

Template.login.events({
    'submit #login-form' : function(e, t){
        e.preventDefault();
        // retrieve the input field values
        var email = $.trim(t.find('#login-email').value);
        var password = $.trim(t.find('#login-password').value);

        Meteor.loginWithPassword(email, password, function(err) {

            if (err){
                 UniUI.setErrorMessage('header',err.message);
            }
            // The user might not have been found, or their passwword
            // could be incorrect. Inform the user that their
            // login attempt has failed.
//            else
            // The user has been logged in.
        });
        return false;
    }
});
