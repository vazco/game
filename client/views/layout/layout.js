'use strict';

Template.layout.onCreated(function(){
    this.subscribe('loggedInUser');
});

Template.layout.helpers({
    loading: function(){
        return Session.get('loading') || false;
    },
    modalsTemplates: function(){
        if(!Router){
            return;
        }
        var current = Router.current();
        if(_.isObject(current) && _.isFunction(current.modalsTemplates)){
            return current.modalsTemplates();
        }
    },
    getPageTitleHeader: function(){
        if(!Router){
            return;
        }
        return Vazco.get(Router.current(), 'pageTitleHeaderView') || 'pageTitleHeaderDefault';
    },
    listingTypeClass: function () {
        var listingType = Session.get('_listingType');
        if (listingType) {
            return 'listing-type-' + Session.get('_listingType');
        }
    },
    getRouteNameForClass: function(){
        return (Vazco.get(Router.current(), 'route.name') || 'default').replace('.', '-');
    }
});

Template.playersInvitations.helpers({
    getInvitations: function(){
        return UniAnyJoin.find({
            possessorId: UniUsers.getLoggedInId(),
            status: UniAnyJoin.STATUS_INVITED
        });
    }
});

Template.playersInvitationsItem.helpers({
    getSubjectTitle: function(){
        var subject = GameRooms.findOne({_id: this.subjectId}) || {};
        console.log('fff', subject, this.subjectId);
        return subject.name || subject.title;
    }
});

Template.playersInvitationsItem.events({
    'click .js-game-join-accept-invitation': function(){
        var subject = this.getSubject();
        subject.joinAcceptInvitation(this.joiningName, function(err){
            if(err){
                if(typeof UniUI !== 'undefined'){
                    UniUI.setErrorMessage('header', err.reason || err.message);
                }
            }
            else{
                Router.go('room', subject);
            }
        });
    },
    'click .js-uni-any-join-resign': function(){
        var subject = this.getSubject();
        subject.joinResign(this.joiningName, UniUsers.getLoggedIn(), function(err){
            if(err){
                if(typeof UniUI !== 'undefined'){
                    UniUI.setErrorMessage('header', err.reason || err.message);
                }
            }
        });
    }
});

Template.header.helpers({
    getInvitationsCount: function(){
        return UniAnyJoin.find({
            possessorId: UniUsers.getLoggedInId(),
            status: UniAnyJoin.STATUS_INVITED
        }).count();
    }
});
