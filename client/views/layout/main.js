'use strict';

Template.mainRooms.helpers({
    getRooms: function(){
        return GameRooms.find(
            {status: GameRooms.STATUS_STARTED},
            {sort: {roomPoints: -1}, limit: 8, fields:{_id:1, name:1, players: 1}});
    },
    getPlayersForCards: function(){
        var players;
        var self = this;
        Tracker.nonreactive(function(){
            players = self.getPlayers().fetch();
        });
        return players;
    }
});

Template.mainRoomCards.helpers({
    getPlayersForCards: function(){
        var players;
        var self = this;
        Tracker.nonreactive(function(){
            players = self.getPlayers().fetch();
        });
        return players;
    }
});