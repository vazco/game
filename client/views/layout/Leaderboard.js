'use strict';

Template.Leaderboard.helpers({
    getUsers: function(){
        return UniUsers.find({}, {sort: {bestGame: -1}}).map(function(user, index){
            user._index = index;
            if(!user.bestGame){
                user.bestGame = 0;
            }
            return user;
        });
    }
});

Template.contestCountdown.onRendered(function(){
    this.$('.js-countdown')
        .countdown('2015/04/16 16:00', function (event) {
            $(this).text(
                event.strftime('%Hh %M:%S')
            );
        });
});