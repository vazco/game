'use strict';

Template.game.onCreated(function () {
    var tmpl = this;
    tmpl.oldMovesId = [];
    var data = Template.currentData() || {};
    var roomId = data.roomId;
    if (roomId && data.user) {
        this.gameRoomId = roomId;
        this.player = data.user;
        this.subscribe('gameMoves', roomId, data.user._id);
    }
    if (roomId && data.user && data.user.isLoggedIn() && !data.blockControl) {
        //var lazy_move = _.throttle(App.game.move, 250, true);
        $(window).on('keydown.game', _.throttle(function (e) {
            if (e.keyCode === GameMoves.LEFT ||
                e.keyCode === GameMoves.RIGHT ||
                e.keyCode === GameMoves.UP ||
                e.keyCode === GameMoves.DOWN) {
                e.preventDefault();
                if(tmpl.isBlock){
                    return;
                }
                tmpl.isBlock = true;
                Meteor.call('moveOn', roomId, e.keyCode, function(err){
                    if(err){
                        console.error('moveOn', err);
                    }
                    _.delay(function(){
                        tmpl.isBlock = false;
                    }, 100);
                });
            }
        }, 550));
        this.view.onViewDestroyed(function () {
            $(window).off('keydown.game');
        });
    }
});

Template.game.onRendered(function () {
    // Start new game
    var tmpl = this;
    var points = new ReactiveVar(0);
    var movingHandle;
    this.autorun(function(c){
        var room = GameRooms.ensureUniDoc(tmpl.gameRoomId);
        var tiles = room && UniUtils.get(room, 'boardCurrentMatrix.'+tmpl.player._id+'.board');
        if(_.isArray(tiles) && tiles.length){
            points.set(UniUtils.get(room, 'boardCurrentMatrix.'+tmpl.player._id+'.points', 0));
            renderBoard(tmpl, tiles);
            App.display.render_next(room.nextTile);
            Tracker.nonreactive(function(){
                movingHandle = Tracker.autorun(function(){
                    var p = points.get();
                    GameMoves.find({
                        roomId: tmpl.gameRoomId, playerId: tmpl.player._id, points: {$gt: p}
                    }, {sort: {points: 1}}).observeChanges({
                        added: function(id, doc){
                            if(_.contains(tmpl.oldMovesId, id)){
                                return;
                            }
                            tmpl.oldMovesId.push(id);
                            if(tmpl.oldMovesId.length > 20){
                                tmpl.oldMovesId.shift();
                            }
                            points.set(doc.points);
                            var g = { board: doc.newTiles, moved: doc.moved };
                            App.display.animate_move(g, doc.direction, tmpl);
                            if(doc.newTile){
                                App.display.animate_new_tile(doc.newTile, doc.direction, tmpl);
                            }

                        }
                    });
                });
            });

            c.stop();
        }
    });
    var handle = GameRooms.find({_id: tmpl.gameRoomId}, {fields: {boardCurrentMatrix: 1}}).observeChanges({
        added: function(id, fields){
            var isEnd = UniUtils.get(fields, 'boardCurrentMatrix.'+tmpl.player._id+'.isEnd');
            if(isEnd){
                tmpl.$('.js-game-board .js-overlay').addClass('active');
            }
        },
        changed: function(id, fields){
            var isEnd = UniUtils.get(fields, 'boardCurrentMatrix.'+tmpl.player._id+'.isEnd');
            if(isEnd){
                if(+tmpl.player.isLoggedIn()){
                    $(window).off('keydown.game');
                }
                tmpl.$('.js-game-board .js-overlay').addClass('active');

                if(movingHandle) {
                    movingHandle.stop();
                }
            }
        }
    });


    this.view.onViewDestroyed(function(){
        if(handle) {
            handle.stop();
        }
        if(movingHandle){
            movingHandle.stop();
        }
    });
});


Template.game.helpers({
    times: function (x) {
        return _.range(x);
    }
});

var renderBoard = function (tmpl, tiles) {
    for (var i = 0; i <= 3; i++) {
        for (var j = 0; j <= 3; j++) {
            var t = tiles[i][j];

            if (tiles[i][j] !== 0) {
                var block = Blaze.toHTMLWithData(Template.tile, {row: i, col: j, tile: t});
                block = $(block).addClass(App.util.tile_class(t));
                // (Here there be magic numbers)
                block = $(block).css({
                    left: 22 + (j * 92),
                    top: 22 + (i * 130)
                });
                tmpl.$('.board').append(block);
            }
        }
    }
};