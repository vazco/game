'use strict';
/****************************
 * Configuration
 ****************************/

Router.configure({
    layoutTemplate: 'layout',
    loadingTemplate: 'loadingView',
    notFoundTemplate: '404',
    templateNameConverter: 'camelCase',
    routeControllerNameConverter: 'upperCamelCase'
});

/********************************
 * Default pages
 ********************************/
Router.map(function() {
    this.route('main', {
        path: '/',
        waitOn: function(){
            return [Meteor.subscribe('gameRooms'), Meteor.subscribe('bestRooms')];
        }
    });
});


Router.map(function() {
    this.route('room.new', {
        path: '/room/new',
        waitOn: function(){
            return Meteor.subscribe('myRooms');
        }
    });
    this.route('room.duel', {
        path: '/rooms/duel',
        waitOn: function(){
            return Meteor.subscribe('gameRooms');
        }
    });
    this.route('leaderboard', {
        path: '/leaderboard',
        waitOn: function(){
            return Meteor.subscribe('getUsersByPoints');
        }
    });
    this.route('room', {
        path: '/room/:_id',
        waitOn: function(){
            return Meteor.subscribe('gameRoom', this.params._id);
        },
        controller: 'BasicController',
        data: function(){
            var room = GameRooms.findOne({_id: this.params._id});
            if(room && room.status === GameRooms.STATUS_STARTED){
                Router.go('room.game', room);
            }
            return room;
        }
    });
    this.route('room.game', {
        path: '/room/:_id/game',
        waitOn: function(){
            return Meteor.subscribe('gameRoom', this.params._id);
        },
        controller: 'BasicController',
        data: function(){
            var data;
            var self = this;
            Tracker.nonreactive(function(){
                data = GameRooms.findOne({_id: self.params._id});
            });
            return data;
        }
    });
});