'use strict';

if (Meteor.isServer) {
    Meteor.startup(function () {
        if (Meteor.users.find({is_admin: true}).count() !== 0) {
            return; // we have admin, nothing to do here
        }

        var admin = {
            username: 'admin',
            email: 'admin@vazco.eu',
            password:  'qwerty',
            profile: {name: 'Administrator'}
        };

        var currentAdmin = Meteor.users.findOne({
            $or: [{
                username: admin.username
            }, {
                emails: { $elemMatch: { address: admin.email } }
            }]
        });
        var adminId = currentAdmin ? currentAdmin._id : Accounts.createUser(admin);

        Meteor.users.update({_id: adminId}, {$set: {is_admin: true}});
    });

}
if (Meteor.isClient) {
    AccountsTemplates.configure({
        homeRoutePath: '/'
    });
    AccountsTemplates.addFields([
        {
            _id: 'name',
            type: 'text',
            displayName: 'Display Name',
            required: true
        }
    ]);
}
