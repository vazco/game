'use strict';

UniCollection.publish('gameRoom', function(id){
    if(!this.userId){
        this.ready();
    }
    this.setMappings(GameRooms, [
        {key: 'players', collection: UniUsers, options: {fields: {profile:1, points:1, permission: 1, is_admin: 1, status:1, bestGame:1}}},
        {key: 'roomId', collection: GameMoves, reverse: true}
    ]);


    return GameRooms.find({_id: id});
});

Meteor.publish('gameMoves', function(roomId, userId){
    if(!this.userId){
        this.ready();
    }
    var sub = this;
    var moves = [];
    var since = new Date();
    var handle = GameMoves.find(
        {roomId: roomId, playerId: userId, createdAt: {$gte: since}},
        {sort: {createdAt: -1}}
    ).observeChanges({
        added: function(id, doc){
            moves.push(id);
            sub.added('gameMoves',id, doc);
            if(moves.length > 20){
                sub.removed('gameMoves', moves.shift());
            }
        }
    });
    this.onStop(function(){
        if(!handle){
            handle.stop();
        }
    });
    this.ready();
});

UniCollection.publish('loggedInUser', function(){
    if(!this.userId){
        this.ready();
    }
    this.setMappings(UniUsers, [{
        key: 'possessorId',
        filter: {status: UniAnyJoin.STATUS_INVITED},
        collection: UniAnyJoin,
        reverse: true
    }]);

    this.setMappings(UniAnyJoin, [{
        key: 'subjectId',
        collection: GameRooms
    }]);

    return UniUsers.find({_id: this.userId}, {fields: {profile:1, points:1, permission: 1, is_admin: 1, status:1, bestGame:1}});
});

Meteor.publish('gameRooms', function(){
    return GameRooms.find({status: {$in: [GameRooms.STATUS_OPEN, GameRooms.STATUS_STARTED]}});
});

Meteor.publish('myRooms', function(){
    return GameRooms.find({ownerId: this.userId, status: GameRooms.STATUS_OPEN});
});

Meteor.publish('getUsersByPoints', function(){
    return UniUsers.find({}, {fields: {points: 1, profile:1, bestGame:1}, sort: {bestGame:-1}});
});

UniCollection.publish('bestRooms', function(){
    if(!this.userId){
        this.ready();
    }
    this.setMappings(GameRooms, [
        {key: 'players', collection: UniUsers, options: {fields: {profile:1, bestGame:1, permission: 1, is_admin: 1, status:1, points:1}}},
        {key: 'roomId', collection: GameMoves, reverse: true}
    ]);
    return GameRooms.find({status: GameRooms.STATUS_STARTED}, {sort: {roomPoints: -1}, limit: 8});
});
