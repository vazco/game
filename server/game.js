'use strict';

App.newGame = function(room) {
    var tiles = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]];

    // Generate new configuration
    var locs = [];
    var count = 0;
    while (count < 9) {
        var row = Math.floor(Math.random() * 4);
        var col = Math.floor(Math.random() * 4);
        if (!_.where(locs, {row: row, col: col}).length) {
            locs.push({row: row, col: col});
            count++;
        }
    }

    _.each(locs, function(l) {
        tiles[l.row][l.col] = App.randomTile(room._id);
    });

    return tiles;
};



var deckInit = [1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3];
var currentDec = {};
App.randomTile = function(roomId, tiles) {
    if(!currentDec[roomId]){
        currentDec[roomId] = {};
    }
    var t, cDeck = currentDec[roomId];
    // Bonus draw
    var bonus = _.some(_.flatten(tiles), function(t) {return (t >= 48);} );
    if (bonus && (Math.random() <= 1/21)) {
        var highest = _.max(_.flatten(tiles));
        var size = Math.log(highest / 3) / Math.log(2) - 3; // Help what is math
        var bonus_deck = _(size).times(function(n) {
            return 6 * Math.pow(2, n);
        });

        t = _.sample(bonus_deck);
        return t;
    }

    // Normal draw
    if (_.isEmpty(cDeck)) {
        cDeck = _.shuffle(deckInit);
    }

    t = _.first(cDeck);
    currentDec[roomId] = _.rest(cDeck);
    return t;
};

App.generateNewBoard = function (direction, tiles) {
    var board = JSON.parse(JSON.stringify(tiles));
    var moved = [];

    var attempt_tile_move = function (i, j, i_pr, j_pr) {
        // Empty space
        if (board[i][j] === 0) {
            return;
        }

        // Twins
        if (board[i][j] === board[i_pr][j_pr]) {
            // Not actually twins
            if (board[i][j] === 1 || board[i][j] === 2) {
                return;
            }

            // Okay actually twins
            board[i][j] = 0;
            board[i_pr][j_pr] *= 2;
            moved.push({i: i, j: j, t: board[i_pr][j_pr]});
        }

        // Not twins
        else {
            // Move to empty space
            if (board[i_pr][j_pr] === 0) {
                board[i_pr][j_pr] = board[i][j];
                board[i][j] = 0;
                moved.push({i: i, j: j, t: board[i_pr][j_pr]});
            }

            // 1 + 2 = 3
            else if ((board[i][j] === 1 && board[i_pr][j_pr] === 2) ||
                (board[i][j] === 2 && board[i_pr][j_pr] === 1)) {
                board[i_pr][j_pr] = 3;
                board[i][j] = 0;
                moved.push({i: i, j: j, t: board[i_pr][j_pr]});
            }
        }
    };
    var i,j;
    switch (direction) {
        case GameMoves.LEFT:
            for (i = 0; i <= 3; i++) {
                for (j = 0; j <= 3; j++) {
                    if (j === 0) {
                        continue;
                    }
                    attempt_tile_move(i, j, i, j - 1);
                }
            }
            break;

        case GameMoves.RIGHT:
            for (i = 0; i <= 3; i++) {
                for (j = 3; j >= 0; j--) {
                    if (j === 3) {
                        continue;
                    }
                    attempt_tile_move(i, j, i, j + 1);
                }
            }
            break;

        case GameMoves.UP:
            for (j = 0; j <= 3; j++) {
                for (i = 0; i <= 3; i++) {
                    if (i === 0) {
                        continue;
                    }
                    attempt_tile_move(i, j, i - 1, j);
                }
            }
            break;

        case GameMoves.DOWN:
            for (j = 0; j <= 3; j++) {
                for (i = 3; i >= 0; i--) {
                    if (i === 3) {
                        continue;
                    }
                    attempt_tile_move(i, j, i + 1, j);
                }
            }
            break;
    }

    return {board: board, moved: moved};
};

App.insertNewTile = function(moved, direction, tiles) {
    var locs = [], j, i, rows, cols;
    switch (direction) {
        case GameMoves.LEFT: // Right column
            j = 3;
            rows = _.uniq(_.pluck(moved, 'i'));

            _.each(rows, function (i) {
                if (tiles[i][j] === 0) {
                    locs.push({i: i, j: j});
                }
            });
            break;

        case GameMoves.RIGHT: // Left column
            j = 0;
            rows = _.uniq(_.pluck(moved, 'i'));

            _.each(rows, function (i) {
                if (tiles[i][j] === 0) {
                    locs.push({i: i, j: j});
                }
            });
            break;

        case GameMoves.UP: // Bottom column
            i = 3;
            cols = _.uniq(_.pluck(moved, 'j'));

            _.each(cols, function (j) {
                if (tiles[i][j] === 0) {
                    locs.push({i: i, j: j});
                }
            });
            break;

        case GameMoves.DOWN: // Top column
            i = 0;
            cols = _.uniq(_.pluck(moved, 'j'));

            _.each(cols, function (j) {
                if (tiles[i][j] === 0) {
                    locs.push({i: i, j: j});
                }
            });
            break;
    }

    return _.sample(locs);
};

App.isEnd = function(tiles) {
    // Check for empty spaces
    var tile_list = _.flatten(tiles);
    if (_.contains(tile_list, 0)) {
        return;
    }
    // Check for moves in every direction
    var directions = [GameMoves.LEFT, GameMoves.RIGHT, GameMoves.UP, GameMoves.DOWN];
    for (var d = 0; d <= 3; d++) {
        var g = App.generateNewBoard(directions[d], tiles);
        if (!_.isEmpty(g.moved)) {
            return;
        }
    }
    return true;
};