'use strict';

Meteor.methods({
    moveOn: function(roomId, direction){
        var user = UniUsers.ensureUniUser();
        var userId = user._id;
        var room = GameRooms.ensureUniDoc(roomId);
        var tiles = UniUtils.get(room, 'boardCurrentMatrix.'+userId+'.board');
        var points = UniUtils.get(room, 'boardCurrentMatrix.'+userId+'.points') || 0;
        if(App.isEnd(tiles)){
            room.endGame(userId);
            return false;
        }
        points += 1;
        var move = {
            playerId: userId,
            roomId: roomId,
            direction: direction,
            points: points
        };
        move.oldTiles = tiles;
        var nb = App.generateNewBoard(direction, tiles);
        if (_.isEmpty(nb.moved)) {
            return false;
        }
        var l = App.insertNewTile(nb.moved, direction, nb.board);
        if(l){
            var t = App.randomTile(room._id);
            nb.board[l.i][l.j] = t;
            move.newTile = {i: l.i, j: l.j, t: t};
        }
        move.moved = nb.moved;
        move.newTiles = nb.board;
        var toSetInRoom = {};
        nb.points = points;
        toSetInRoom['boardCurrentMatrix.'+userId+''] = nb;
        if(!user.bestGame){
            user.bestGame = 0;
        }
        var maxPoints = Math.max(points, user.bestGame);
        UniUsers.update({_id: userId},{$inc: {points: 1}, $set: {bestGame: maxPoints}});
        room.update({$set: toSetInRoom, $inc: {roomPoints: 1}});
        GameMoves.insert(move);
        if(App.isEnd(tiles)){
            room.endGame(userId);
        }
    }
});
