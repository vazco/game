'use strict';


(Meteor.isClient?window:global).GameRooms = new UniCollection('gameRooms');

GameRooms.STATUS_OPEN = 'open';
GameRooms.STATUS_STARTED = 'started';
GameRooms.STATUS_CLOSED = 'closed';
GameRooms.STATUS_END = 'ends';

GameRooms.attachSchema(new SimpleSchema(_.extend(UniCollection.getBasicSchema(), {
    name: {
        type: String
    },
    players: {
        type: [String],
        autoValue: function() {
            if (this.isInsert || this.isUpsert) {
                return [];
            }
        },
        optional: true,
        autoform: {
            omit: true
        }
    },
    status: {
        type: String,
        autoValue: function() {
            if (this.isInsert || this.isUpsert) {
                return GameRooms.STATUS_OPEN;
            }
        },
        allowedValues: [
            GameRooms.STATUS_OPEN,
            GameRooms.STATUS_STARTED,
            GameRooms.STATUS_CLOSED,
            GameRooms.STATUS_END
        ],
        autoform: {
            omit: true
        }
    },
    boardCurrentMatrix:{
        type: Object,
        optional: true,
        blackbox: true,
        autoform: {
            omit: true
        }
    },
    roomPoints:{
        type: Number,
        defaultValue: 0,
        optional: true,
        autoform: {
            omit: true
        }
    }

})));

GameRooms.helpers({
    getPlayers: function(){
        if(_.isArray(this.players) && this.players.length){
            return UniUsers.find({_id: {$in: this.players}});
        }
    },
    startGame: function(){
        this.update({$set:{status: GameRooms.STATUS_STARTED}});
    },
    closeGame: function(){
        this.update({$set:{status: GameRooms.STATUS_CLOSED}});
    },
    endGame: function(playerId){
        if(playerId){
            var toSet = {};
            toSet['boardCurrentMatrix.'+playerId+'.isEnd'] = true;
            console.log('ENDING GAME ', playerId);
            this.update({$set: toSet});
        }
        this.refresh();
        if(_.every(this.boardCurrentMatrix, function(conf){
                return conf.isEnd;
            })){
            console.log('ENDING ROOM', this._id);
            this.update({$set:{status: GameRooms.STATUS_END}});
        }

    }
});

var _addNewPlayerCallBack = function (joiningName, uniAnyJoinDocument) {
    this.update({$addToSet: {players: uniAnyJoinDocument.possessorId}});
};

var _removePlayerCallBack = function (joiningName, uniAnyJoinDocument) {
    if(this.ownerId === uniAnyJoinDocument.possessorId){
        this.closeGame();
    }
    this.update({$pull: {players: uniAnyJoinDocument.possessorId}});
};

GameRooms.attachAnyJoin('players', {
    onJoin: _addNewPlayerCallBack,
    onResign: _removePlayerCallBack,
    onAcceptInvitation: _addNewPlayerCallBack,
    onAcceptRequest: _addNewPlayerCallBack,
    onGetDefaultPolicy: function(){
        return UniAnyJoin.TYPE_JOIN_OPEN;
    }
});

GameRooms.allow({
    insert: function(userId, doc){
        return !!userId && !doc.boardCurrentMatrix;
    },
    update: function(userId, room, fields){
        if(_.contains(fields, 'boardCurrentMatrix')){
            return false;
        }
        return room.ownerId === userId || UniUsers.isAdminLoggedIn();
    },
    publish: function(){
        return true;
    }
});

if (Meteor.isServer) {
    GameRooms.after.insert(function (userId) {
        this.transform().join('players', userId, userId);
    });
    GameRooms.after.update(function(userId, room, fieldNames){
        if(_.contains(fieldNames, 'status') && this.previous.status === GameRooms.STATUS_OPEN &&
            room.status === GameRooms.STATUS_STARTED){
            var tiles = App.newGame(room);
            var currentUsers = {};
            _.each(room.players, function(pId){
                currentUsers[pId] = {board: tiles, end: false};
            });
            this.transform().update({
                $set: {
                    boardCurrentMatrix: currentUsers
                }
            });
        }
    });
} else{
    GameRooms.addErrorSupportToAllWriteMethods();
}
