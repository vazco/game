'use strict';

(Meteor.isClient?window:global).GameMoves = new UniCollection('gameMoves');

GameMoves.LEFT = 37;
GameMoves.RIGHT = 39;
GameMoves.UP = 38;
GameMoves.DOWN = 40;

GameMoves.attachSchema({
    createdAt: {
        type: Date,
        autoValue: function() {
            if (this.isInsert) {
                return new Date();
            }
        },
        denyUpdate: true
    },
    roomId: {
        type: String
    },
    playerId: {
        type: String
    },
    direction: {
        type: Number
    },
    points: {
        type: Number,
        optional: true
    },
    oldTiles: {
        type: Array,
        minCount: 4,
        maxCount: 4,
        optional: true
    },
    'oldTiles.$': {
        type: [Number],
        minCount: 4,
        maxCount: 4
    },
    newTiles: {
        type: Array,
        minCount: 4,
        maxCount: 4,
        optional: true
    },
    'newTiles.$': {
        type: [Number],
        minCount: 4,
        maxCount: 4
    },
    moved: {
        type:[Object],
        optional: true
    },
    'moved.$.i': {
        type: Number,
        optional: true
    },
    'moved.$.j': {
        type: Number,
        optional: true
    },
    'moved.$.t': {
        type: Number,
        optional: true
    },
    newTile: {
        type: Object,
        blackbox: true
    }

});

GameMoves.allow({
    insert: function(userId, doc){
        return userId && (userId === doc.playerId || UniUsers.isAdminLoggedIn());
    },
    update: function(){
        return false;
    },
    publish: function(){
        return true;
    }
});


GameMoves.setDefaultSort({points: -1});