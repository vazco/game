'use strict';

UniUsers.allow({
    update: function (userId, doc, fieldNames) {
        if (userId && doc._id === userId && _.indexOf(fieldNames, 'is_admin') === -1) {
            return true;
        }
        // admin can modify any
        return (Meteor.userId() && Meteor.user().is_admin);
    },
    publish: function(){
        return true;
    }
});





UniAnyJoin.allow({
    publish: function(){
        return true;
    }
});